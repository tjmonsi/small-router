declare module 'is-invalid-path';

interface AddFilePrompt {
  destinationpath: string,
  filename: string,
  description: string
}

interface AddDocumentPagePrompt {
  name: string,
  destinationpath: string | null,
  isIndex: boolean,
  weight: number
}
