import { SmallRouter } from './src/small-router.js';
import { SmallQuery } from './src/small-query.js';
import { SmallLocation } from './src/small-location.js';

window.customElements.define('small-router', SmallRouter);
window.customElements.define('small-query', SmallQuery);
window.customElements.define('small-location', SmallLocation);
