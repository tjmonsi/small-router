import { LitElement, html } from 'lit';

class TestComponent extends LitElement {
  render () {
    return html`Test component <a href="/demo">Back</a>`;
  }
}

window.customElements.define('test-component', TestComponent);
