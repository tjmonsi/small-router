# @tjmonsi/small-router [![pipeline status](https://gitlab.com/tjmonsi/small-router/badges/master/pipeline.svg)](https://gitlab.com/tjmonsi/small-router/-/commits/master)[![coverage report](https://gitlab.com/tjmonsi/small-router/badges/master/coverage.svg)](https://gitlab.com/tjmonsi/small-router/-/commits/master)

small-router

Page Documentation can be found at: https://tjmonsi.gitlab.io//small-router/

This webcomponent follows the [open-wc](https://github.com/open-wc/open-wc) recommendation.

## Installation

```bash
npm i small-router
```

## Usage

Load the module, then get the element via `querySelector`.

Add a `routes` object, which is a data dictionary object where
the keys are routes and the values are objects with 4 properties:
- `render`: this is the html that you want to render when the particular route has been matched to the url.
- `postRender`: this can be a `function` or a `promise` or an `array` of `functions` or `promises` that will be called AFTER the render has been done. Take note that the ordering will be followed, but the promises are done almost simultaneously (they are not dependent on each other). If one promise fails, it will all fail.
- `preRender`: this can be a `function` or a `promise` or an `array` of `functions` or `promises` that will be called BEFORE the render will be done. This can be used to check for authorization for a page. ake note that the ordering will be followed, but the promises are done almost simultaneously (they are not dependent on each other). If one promise fails, it will all fail. If it fails, it will go back to the fallbackUrl. If the function returns `true`, it will also fail.
- `fallbackUrl`: the path that it will go to if the `preloadChecks` returns an error or returns `true`.

There is a special route called `not-found`, which can be a fallback for 404 not found.

```html
<small-router></small-router>

<script type="module">
  import 'small-router/small-router.js';

  const router = document.querySelector('small-router');
  router.routes = {
    '/demo': {
      render: () => html`Hello <a href="./demo1">world</a> - <a href="/demo2">haha</a>`
    },
    '/demo1': {
      render: () => html`<test-component></test-component>`,
      preRender: async () => import('./test-component.js'),
      fallbackUrl: '/demo'
    },
    '/demo2': {
      preRender: () => 'some error',
      fallbackUrl: '/demo1'
    }
  };

  router.addEventListener('route-change-error', function ({ detail }) {
    console.log('error when changing routes', detail);
  });
</script>
```

## Linting and formatting

To scan the project for linting and formatting errors, run

```bash
npm run lint
```

To automatically fix linting and formatting errors, run

```bash
npm run format
```

## Testing with Web Test Runner

To execute a single test run:

```bash
npm run test
```

To run the tests in interactive watch mode run:

```bash
npm run test:watch
```

## Demoing with Storybook

To run a local instance of Storybook for your component, run

```bash
npm run storybook
```

To build a production version of Storybook, run

```bash
npm run storybook:build
```


## Tooling configs

For most of the tools, the configuration is in the `package.json` to minimize the amount of files in your project.

If you customize the configuration a lot, you can consider moving them to individual files.

## Local Demo with `web-dev-server`

```bash
npm start
```

To run a local development server that serves the basic demo located in `demo/index.html`

# Installation

You should have the following:
- Node version 12 or 14

To install run,
```bash
npm install
```

# Usage

To run this project

```bash
npm start
```

# Documentation

Please visit the repo page or if you want to build your own copy here, you have to install Hugo globally

```bash
brew install hugo
```

Then run the following commands:

```bash
npm run serve:docs
```

If you want to just build the docs,

```bash
npm run build:docs
```
