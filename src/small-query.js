import { LitElement } from 'lit';

class SmallQuery extends LitElement {
  /** @param {string} value */
  // eslint-disable-next-line accessor-pairs
  set query (value) {
    const oldValue = this.__query;
    this.__query = value;
    this._queryChanged(value);
    this.requestUpdate('query', oldValue);
  }

  get query () {
    return this.__query;
  }

  /** @param {object} value */
  set queryObject (value) {
    const oldValue = this.__queryObject;
    this.__queryObject = value;
    this._queryObjectChanged(value);
    this.requestUpdate('queryObject', oldValue);
  }

  get queryObject () {
    return this.__queryObject;
  }

  static properties = {
    query: { type: String },
    queryObject: { type: Object }
  }

  constructor () {
    super();
    this.__query = '';
    this.__queryObject = {};
  }

  connectedCallback () {
    super.connectedCallback();
    this.queryObject = {};
    this._dontReact = false;
    this._dontReactQuery = false;
  }

  /**
   *
   * @param {string} query
   */
  _queryChanged (query) {
    if (this._dontReactQuery) return;
    this._dontReact = true;
    const queryObject = this.decodeParams(query);
    this.queryObject = queryObject;
    Promise.resolve().then(() =>
      this.dispatchEvent(new window.CustomEvent('query-object-change', { detail: queryObject })));

    this._dontReact = false;
  }

  /**
   *
   * @param {Object<string, *>} queryObject
   */
  _queryObjectChanged (queryObject) {
    if (this._dontReact) return;
    this._dontReactQuery = true;
    const query = this.encodeParams(queryObject)
      .replace(/%3F/g, '?')
      .replace(/%2F/g, '/')
      .replace(/'/g, '%27');
    this.query = query;
    Promise.resolve().then(() =>
      this.dispatchEvent(new window.CustomEvent('query-change', { detail: query })));

    this._dontReactQuery = false;
  }

  /**
   *
   * @param {Object<string, *>} params
   */
  encodeParams (params) {
    const encodedParams = [];
    for (const key in params) {
      // eslint-disable-next-line
      const value = params[key];
      if (value === '') {
        encodedParams.push(window.encodeURIComponent(key));
      } else if (value) {
        encodedParams.push(`${window.encodeURIComponent(key)}=${window.encodeURIComponent(value.toString())}`);
      }
    }
    return encodedParams.join('&');
  }

  /**
   *
   * @param {string} paramString
   */
  decodeParams (paramString) {
    /**
     * @type {Object<string, *>}
     */
    const params = {};

    paramString = (paramString || '').replace(/\+/g, '%20');
    const paramList = paramString.split('&');
    for (const paramItem of paramList) {
      const [key, value] = paramItem.split('=');
      if (key) {
        params[window.decodeURIComponent(key)] = window.decodeURIComponent(value);
      }
    }
    return params;
  }
}

export { SmallQuery };
