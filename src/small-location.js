import { LitElement } from 'lit';
import { resolveUrl } from './lib/resolve-url.js';

class SmallLocation extends LitElement {
  /** @param {string} value */
  set urlSpaceRegex (value) {
    const oldValue = this.__urlSpaceRegex;
    this.__urlSpaceRegex = value;
    this._urlSpaceRegExp = RegExp(value);
    this.requestUpdate('urlSpaceRegex', oldValue);
  }

  get urlSpaceRegex () {
    return this.__urlSpaceRegex;
  }

  static properties = {
    urlSpaceRegex: { type: String },
    path: { type: String },
    query: { type: String },
    hash: { type: String }
  }

  constructor () {
    super();
    this.__urlSpaceRegex = '';
    this._boundHashChanged = this._hashChanged.bind(this);
    this._boundUrlChanged = this._urlChanged.bind(this);
    this._boundGlobalOnClick = this._globalOnClick.bind(this);
    /** @type {RegExp?} */
    this._urlSpaceRegExp = null;

    this.dwellTime = 2000;
  }

  connectedCallback () {
    super.connectedCallback();

    window.addEventListener('hashchange', this._boundHashChanged);
    window.addEventListener('location-change', this._boundUrlChanged);
    window.addEventListener('popstate', this._boundUrlChanged);

    document.body.addEventListener('click', this._boundGlobalOnClick, true);

    if (window.performance) {
      /** @type {number?} */
      this._lastChangedAt = window.performance.now() - (this.dwellTime - 200);
    }

    this._initialized = true;
    this._urlChanged();
  }

  disconnectedCallback () {
    if (super.disconnectedCallback && typeof super.disconnectedCallback === 'function') {
      super.disconnectedCallback();
    }

    window.removeEventListener('hashchange', this._boundHashChanged);
    window.removeEventListener('location-change', this._boundUrlChanged);
    window.removeEventListener('popstate', this._boundUrlChanged);
    document.body.removeEventListener('click', this._boundGlobalOnClick);
    this._initialized = false;
  }

  /**
   *
   * @param {*} changedProperties
   */
  updated (changedProperties) {
    super.updated(changedProperties);
    this._updateUrl();
  }

  _hashChanged () {
    const hash = window.decodeURIComponent(window.location.hash.slice(1));
    this.hash = hash;
    Promise.resolve().then(() =>
      this.dispatchEvent(new window.CustomEvent('hash-change', { detail: hash })));
  }

  _urlChanged () {
    this._dontUpdateUrl = true;
    this._hashChanged();

    const path = window.decodeURIComponent(window.location.pathname);
    const query = window.location.search.slice(1);

    this.path = path;
    this.query = query;

    Promise.resolve().then(() => {
      this.dispatchEvent(new window.CustomEvent('path-change', { detail: path }));
      this.dispatchEvent(new window.CustomEvent('query-change', { detail: query }));
    });

    this._dontUpdateUrl = false;
  }

  _getUrl () {
    const partiallyEncodedPath = window
      // @ts-ignore
      .encodeURI(this.path)
      .replace(/#/g, '%23')
      .replace(/\?/g, '%3F');

    const partiallyEncodedQuery = this.query ? `?${this.query.replace(/#/g, '%23')}` : '';
    const partiallyEncodedHash = this.hash ? `#${window.encodeURI(this.hash)}` : '';

    return `${partiallyEncodedPath}${partiallyEncodedQuery}${partiallyEncodedHash}`;
  }

  _updateUrl () {
    if (this._dontUpdateUrl || !this._initialized) {
      return;
    }

    if (this.path === window.decodeURIComponent(window.location.pathname) &&
        this.query === window.location.search.substring(1) &&
        this.hash === window.decodeURIComponent(window.location.hash.substring(1))) {
      // Nothing to do here
      return;
    }

    const newUrl = this._getUrl();
    const { href: fullNewUrl } = resolveUrl(newUrl, `${window.location.protocol}//${window.location.host}`);
    const now = window.performance ? window.performance.now() : null;
    const shouldReplace = this._lastChangedAt && now ? (this._lastChangedAt + this.dwellTime) > now : false;
    this._lastChangedAt = now || this._lastChangedAt;
    if (shouldReplace) {
      window.history.replaceState({}, '', fullNewUrl);
    } else {
      window.history.pushState({}, '', fullNewUrl);
    }
    window.dispatchEvent(new window.CustomEvent('location-change'));
  }

  /**
   *
   * @param {MouseEvent} event
   */
  _globalOnClick (event) {
    if (event.defaultPrevented) {
      return;
    }

    const href = this._getSameOriginLinkHref(event);

    if (!href) {
      return;
    }

    event.preventDefault();

    if (href === window.location.href) {
      return;
    }

    window.history.pushState({}, '', href);
    window.dispatchEvent(new window.CustomEvent('location-change'));
  }

  /**
   *
   * @param {MouseEvent} event
   * @return {string | null}
   */
  _getSameOriginLinkHref (event) {
    if (event.button !== 0) {
      return null;
    }

    if (event.metaKey || event.ctrlKey) {
      return null;
    }

    const eventPath = event.composedPath();
    let anchor = null;

    for (const el of eventPath) {
      const element = (/** @type {HTMLAnchorElement} */ (el));
      if (element.tagName === 'A' && (element).href) {
        anchor = element;
        break;
      }
    }

    if (!anchor) {
      return null;
    }

    if (anchor.target === '_blank') {
      // @ts-ignore
      const { ga } = window;
      if (anchor.href && ga) {
        ga('send', 'event', 'Link', 'Click', anchor.href, 1);
      }
      return null;
    }

    if ((anchor.target === '_top' || anchor.target === '_parent') && window.top !== window) {
      return null;
    }

    const { href } = anchor;
    const url = document.baseURI !== null
      ? resolveUrl(href, document.baseURI)
      : resolveUrl(href, '');

    const origin = window.location.origin || `${window.location.protocol}//${window.location.host}`;

    const urlOrigin = url.origin || `${url.protocol}//${url.host}`;

    if (urlOrigin !== origin) {
      return null;
    }

    let normalizedHref = `${url.pathname}${url.search}${url.hash}`;
    if (normalizedHref[0] !== '/') {
      normalizedHref = `/${normalizedHref}`;
    }

    if (this._urlSpaceRegExp && !this._urlSpaceRegExp.test(normalizedHref)) {
      return null;
    }

    const { href: fullNormalizedHref } = resolveUrl(normalizedHref, window.location.href);
    return fullNormalizedHref;
  }
}

export { SmallLocation };
