import { LitElement, html } from 'lit';
import { pathToRegexp } from 'path-to-regexp';

/**
 * @param {string} str1
 * @param {string} str2
 * @returns {boolean}
 */
// const checkStringEquality = (str1, str2) => {
//   let mismatch = 0;
//   for (let i = 0; i < str1.length; ++i) {
//     mismatch |= (str1.charCodeAt(i) ^ str2.charCodeAt(i));
//   }
//   return mismatch === 0;
// };

export class SmallRouter extends LitElement {
  static get properties () {
    return {
      fallbackRoute: {
        type: String,
        attribute: 'fallback-route'
      },
      currentRoute: {
        type: String
      },
      path: {
        type: String
      },
      hash: {
        type: String
      },
      query: {
        type: String
      },
      routes: {
        type: Object
      },
      renderPage: {
        type: Object
      }
    };
  }

  /**
   * @param {string} newValue
   */
  set path (newValue) {
    const oldValue = this.__path;
    this.__path = newValue;
    this._pathChanged(newValue);
    this.requestUpdate('path', oldValue);
  }

  get path () {
    return this.__path;
  }

  /**
   * @param {string} newValue
   */
  set query (newValue) {
    const oldValue = this.__query;
    if (this.smallQuery) {
      this.smallQuery.query = newValue;
    }
    this.requestUpdate('query', oldValue);
  }

  get query () {
    return this.__query;
  }

  constructor () {
    super();

    this.__path = '/';
    this.__query = '';

    /** @type {*} */
    // this.appendedElement = null;

    // initialize variables
    /** @type {string} */
    // this.container = '#main-container';

    /** @type {HTMLElement} */
    // this.body = document;

    /** @type {string?} */
    this.componentName = null;

    // this._boundSmallPathChanged = this._smallPathChanged.bind(this);
    // this._boundSmallHashChanged = this._smallHashChanged.bind(this);
    // this._boundSmallQueryChanged = this._smallQueryChanged.bind(this);
    // this._boundSmallQueryObjectChanged = this._smallQueryObjectChanged.bind(this);

    /** @type {*} */
    this.routes = {};
    this._routeInitialized = false;
    this.paramObject = {};
    this.queryObject = {};

    /** @type {*} */
    this.hash = null;
  }

  connectedCallback () {
    super.connectedCallback();

    this.smallLocation = /** @type {import('./small-location.js').SmallLocation} */(document.createElement('small-location'));
    this.smallQuery = /** @type {import('./small-query').SmallQuery} */(document.createElement('small-query'));

    const parent = this.parentNode;
    parent?.insertBefore(this.smallLocation, this);
    parent?.insertBefore(this.smallQuery, this);

    const el = this;

    // @ts-ignore
    this.smallLocation.addEventListener('path-change', function ({ detail }) {
      el.path = detail;
    });
    // @ts-ignore
    this.smallLocation.addEventListener('hash-change', function ({ detail }) {
      el.hash = detail;
    });
    // @ts-ignore
    this.smallLocation.addEventListener('query-change', function ({ detail }) {
      el.query = detail;
    });
    // @ts-ignore
    this.smallQuery.addEventListener('query-object-change', function ({ detail }) {
      el.queryObject = detail;
    });

    if (!this.fallbackRoute) this.fallbackRoute = 'not-found';

    this._routeInitialized = true;
    if (this.path) this._pathChanged(this.path);
  }

  createRenderRoot () {
    return this;
  }

  render () {
    return html`${this.renderPage}`;
  }

  /**
   *
   * @param {string} url
   */
  changeUrl (url) {
    window.history.pushState({}, '', url);
    window.dispatchEvent(new window.CustomEvent('location-change'));
  }

  /**
   *
   * @param {*} param
   */
  // _smallPathChanged ({ detail: path }) {
  //   if (this.path !== path) {
  //     this.path = path;
  //     if (this.appendedElement) {
  //       this.appendedElement.path = path;
  //     }
  //   }
  // }

  /**
   *
   * @param {*} param
   */
  // _smallHashChanged ({ detail: hash }) {
  //   if (!checkStringEquality(this.hash, hash)) {
  //     this.hash = hash;
  //     if (this.appendedElement) {
  //       this.appendedElement.hash = hash;
  //     }
  //   }
  // }

  /**
   *
   * @param {*} param
   */
  // _smallQueryChanged ({ detail: query }) {
  //   if (this.query !== query) {
  //     this.query = query;
  //     if (this.appendedElement) {
  //       this.appendedElement.query = query;
  //     }
  //   }
  // }

  /**
   *
   * @param {*} param
   */
  // _smallQueryObjectChanged ({ detail: queryObject }) {
  //   this.queryObject = queryObject;
  //   if (this.appendedElement) {
  //     this.appendedElement.queryObject = queryObject;
  //   }
  // }

  /**
   *
   * @param {*} path
   */
  _pathChanged (path = '/') {
    let exec = null;
    let re = null;
    /**
     * @type {Array<*>}
     */
    let keys = [];

    if (this.routes && Object.keys(this.routes).length < 1) {
      return;
    }

    // add a route if not-found is not in the list
    if (!this.routes['not-found']) {
      this.routes['not-found'] = {
        render: () => html`404 Not Found`
      };
    }

    for (const route of Object.keys(this.routes)) {
      keys = [];
      re = pathToRegexp(route, keys);

      exec = re.exec(path);

      if (exec) return this._routeMatched(route, exec, keys);
    }

    return this._routeMatched(this.fallbackRoute, [], []);
  }

  /**
   * @param {*} scripts
   * @param {*} params
   * @param {string} fallbackUrl
   */
  async _runScripts (scripts, params, fallbackUrl) {
    const promises = [];
    const scriptsArray = typeof scripts === 'function'
      ? [scripts]
      : scripts;
    try {
      for (const fn of scriptsArray) {
        if (typeof fn === 'function') {
          promises.push(fn(params));
        }
      }

      await Promise.all(promises);
      return true;
    } catch (error) {
      console.error(error);
      this.dispatchEvent(new window.CustomEvent('route-change-error', {
        detail: error
      }));
      this.changeUrl(typeof fallbackUrl === 'string'
        ? fallbackUrl
        : '/');
      return false;
    }
  }

  /**
   *
   * @param {*} currentRoute
   * @param {Array<*>} exec
   * @param {Array<*>} keys
   */
  async _routeMatched (currentRoute, exec, keys) {
    /**
     * @type {Object<string, *>}
     */
    const paramObject = {};
    for (let i = 0; i < keys.length; i++) {
      // eslint-disable-next-line
      const key = keys[i];
      const { name } = key;
      paramObject[`${name}`] = exec[i + 1] || null;
    }

    this.paramObject = paramObject;
    this.currentRoute = currentRoute;

    await this.updateComplete;

    this.dispatchEvent(new window.CustomEvent('param-object-change', {
      detail: paramObject
    }));
    this.dispatchEvent(new window.CustomEvent('current-route-change', {
      detail: currentRoute
    }));

    if (!this.routes || !this.routes[`${currentRoute}`]) {
      console.error(`No route found for ${currentRoute}`);
    }

    const {
      preRender, // do a function / promise in order - used to lazy load scripts
      render, // render function using lit-html or just a string to be rendered
      postRender, // do a function / promise after rendering
      fallbackUrl // a fallback url if preloadChecks returns false or an error has occurred (returned a string which is an error message)
    } = this.routes[`${currentRoute}`] || {};

    const params = {
      params: this.paramObject,
      query: this.queryObject,
      hash: this.hash,
      path: this.path
    };

    if (preRender) {
      const result = await this._runScripts(preRender, params, fallbackUrl);
      // if the _runScripts returned false, it should not render
      if (!result) return;
    }

    if (render && typeof render === 'function') {
      this.renderPage = await render(params);
    }

    if (postRender) {
      await this._runScripts(postRender, params, fallbackUrl);
    }
  }
}
